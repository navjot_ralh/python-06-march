d={}
# Add elements in dictionary
d['name']='Shinchan'
d['last_name']='Nohara'

d.update({'pet_name':'Sheero'})
d.update({'age':5})

# Update values in dictionary
d['name']='Harry'

# Count number of elements
print(len(d))

# Delete elements
del d['pet_name']

# Count number of elements
print(len(d))

# Search for a key
print('name' in d)
print('name' in d.keys())
print('Harry' in d.values())

# Create a new dictionary from set of keys
newd={}
t=('name','salary','leaves','age')
newd=newd.fromkeys(t,100)
print(newd)

# Fetch keys, values and pairs
print(d.keys())                                 # All the keys of dictionary
print(d.values())                               # All the values of the keys
print(d.items())                                # All the key:value pairs

# Clear a dictionary
# d.clear()
# print(d)

# Copy a Dictionary
n=d.copy()
print(n)