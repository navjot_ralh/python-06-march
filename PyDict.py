# key:value pairs; mutable
student={                     # Dictionary; object
    'name':'Navi',
    'roll_no':1,
    'class':'Python',
    'subjects':['HTML','CSS','Python','JS'],
    'expenses':[10.0,20.5,1000,5000]
}
# print(student['name'])
# print(student['roll_no'])
# print(type(student))
# print(type(student['roll_no']))
# print(student['subjects'][2])

for i in student:
    print(i,'=> ',student[i])

ex=student['expenses']
s=0
for i in ex:
    s+=i
print('Total Expense:',s)

# student=[{                      # Array
#     'name':'Navi',
#     'roll_no':1,
#     'class':'Python',
#     'subjects':['HTML','CSS','Python','JS']
# }]
# print(student[0])